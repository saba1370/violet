import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import { TraderService, TraderUpdate } from 'src/model/trader-service';
import { Trader } from 'src/model/trader';
import { strictEqual } from 'assert';
import { stringify } from 'querystring';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit, TraderUpdate {
  traders: Trader[]
  totalProfit:number = 0
  constructor(private service: TraderService) {
    this.service = service;
    this.service.subscribe(this);
    this.service.notify();
    
  }
  
  ngOnInit() {
  }
  
  latestTraders(traders: Array<Trader>) {
    this.traders = traders;
    this.generateData(traders);
    this.generateLineChartData(traders);
  }
  
  nextColorIndex = 0
  
  generateLineChartData(traders: Trader[]){
    
    this.lineChartData[0].data= [
    ] 
    
    this.lineChartLabels = [];

    let profit = {}
    traders.forEach((trader)=>{
      trader.positions.forEach((position)=>{
        if(position.closingTrade != null){
          let date = new Date(position.closingTrade.when)
          console.log(date.getMonth())
          let key = date.getFullYear()+'-' + (date.getMonth()+1).toString().padStart(2, '0')+'-' + date.getDate().toString().padStart(2, '0')
          if(key in profit){
            profit[key]+=position.profitOrLoss
          }else{
            profit[key] = position.profitOrLoss
          }
        }
      })

    })
    let keys = Object.keys(profit).sort()
    keys.forEach((key)=>{
        this.lineChartLabels.push(key)
        this.lineChartData[0].data.push(profit[key])
    })
    console.log("asdasd")
    console.log(profit)


  }
  generateData(traders: Trader[]){
    
    let portfolio = {}
    let totalTrades = 0
    this.totalProfit = 0
    console.log(traders)
    traders.forEach((trader)=>{
      totalTrades += trader.positions.length
      if(trader.stock in portfolio ){
        portfolio[trader.stock] += trader.positions.length
      }else{
        portfolio[trader.stock] = trader.positions.length
      }
      
      this.totalProfit += trader.profitOrLoss
    })
    
    
    
    
    console.log(portfolio)
    this.pieChartLabels = []
    this.pieChartData =[]
    this.pieChartColors[0].backgroundColor =[]
    for(var key in portfolio){
      this.pieChartLabels.push(key)
      this.pieChartData.push(Math.round(portfolio[key]*100/totalTrades))
      this.pieChartColors[0].backgroundColor.push(this.getNextColor())
    }
    
  }
  getNextColor(){
    this.nextColorIndex++
    return this.colors[this.nextColorIndex%this.colors.length]
  }
  // color 
  public colors:string[] =  ['rgba(255,0,0,0.3)', 'rgba(0,255,0,0.3)', 'rgba(0,0,255,0.3)' ,'rgba(196,79,244,0.3)']
  // Pie
  public pieChartLabels:string[] = [];
  public pieChartData:number[] = [];
  public pieChartType:string = 'pie';
  public pieChartColors = [
    {
      backgroundColor: [],
    },
  ];
  
  
  // line
  public lineChartData: ChartDataSets[] = [
    { data: [], label: 'daily profit' }
  ] 
  
  public lineChartLabels: Label[] = [];
  public lineChartType:string = 'line';
  public lineChartOptions: (ChartOptions) = {
    responsive: true,
    scales: {
      // We use this empty structure as a placeholder for dynamic theming.
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          ticks: { 
            fontColor: 'red',
          }
        }
      ]
    },
    
  };
  
  
  // events
  public chartClicked(e:any):void {
    console.log(e);
  }
  public chartHovered(e:any):void{
    console.log(e);
  }
  public pieChartHovered(e:any):void {
    console.log(e);
  }
  
  public getPortfolio(){
    
  }
  
}

