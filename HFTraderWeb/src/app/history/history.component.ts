import { Component, OnInit, Input } from '@angular/core';
import { Trader } from "../../model/trader";
import { TraderService } from "../../model/trader-service";
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-history', 
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  @Input() trader: Trader;
  positions;
  service: TraderService;

  constructor(public activeModal: NgbActiveModal) {
  }
   ngOnInit() {
   }
}
