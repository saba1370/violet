import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { PricePoint } from 'src/model/price-point';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.css']
})
export class PricingComponent implements OnInit {
  @Input() pricePoints: Array<PricePoint>;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}



