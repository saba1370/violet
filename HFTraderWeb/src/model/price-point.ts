export class PricePoint {
    id: number;
    stock: string;
    timestamp
    open: number;
    high: number;
    low: number;
    close: number;
    volume: number; 

    constructor(id: number, stock: string,
        timestamp, open: number,
        high: number, low: number,close: number,
        volume: number, ) {
      this.id = id;
      this.stock = stock;
      this.timestamp = timestamp;
      this.open = open;
      this.low = low;
      this.high = high;
      this.close = close;
      this.volume = volume;
    }

}
