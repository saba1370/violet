package com.citi.trading.pricing;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.citi.trading.strategy.Strategy;
import com.citi.trading.strategy.StrategyRepository;
import com.citi.trading.strategy.TraderService;

@RestController
@RequestMapping("/pricing")
@CrossOrigin(origins="http://localhost:4200")
public class PricingService {
	
	@Autowired
	private PricePointRepository pricePointRepository;
	
	private static final Logger LOGGER =
	        Logger.getLogger(PricingService.class.getName () + "Violet");
	
	@GetMapping("/")
	public String index(){
		return "/index.html";
	}
	
//	@GetMapping("{Stock}")
//	public PricePoint getLatestPricePoint(@PathVariable("Stock") String stock) {
//		PricePoint result = pricePointRepository.findLatestPricePoint(stock);
//		return result;
//	}
	
	@GetMapping("{Stock}/all")
	public List<PricePoint> getAllPricePoints(@PathVariable("Stock") String stock) {
		ArrayList<PricePoint> result = new ArrayList<>();
		for (PricePoint pricePoint : pricePointRepository.findByStock(stock)) {
			result.add(pricePoint);
		}
		LOGGER.info("Returning " + result.toString());
		return result;
		
	}
	
	@GetMapping("{Stock}/{Start}/{End}")
	public List<PricePoint> getPricePointsByTime(@PathVariable("Stock") String stock, @PathVariable("Start") Timestamp start, @PathVariable("End") Timestamp end ) {
		return pricePointRepository.findByStockAndRange(stock, start, end);
	}
}
