package com.citi.pricing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class PriceServiceApplication {

	@Bean
	public BasePrices basePrices() {
		return new BasePrices("BasePrices.csv");
	}
	
	public static void main(String[] args) {
		SpringApplication.run(PriceServiceApplication.class, args);
	}
}
